<?php

if ( ! defined( '_S_VERSION' ) ) {
	define( '_S_VERSION', '1.0.3' );
}

add_theme_support( 'post-thumbnails' );
add_filter('use_block_editor_for_post', '__return_false', 10);


function rav_scripts() {
	wp_enqueue_style( 'rav-style', get_stylesheet_uri(), array(), _S_VERSION );
    wp_enqueue_style( 'swiper-css', 'https://cdn.jsdelivr.net/npm/swiper@8/swiper-bundle.min.css', array(), _S_VERSION );
    //wp_enqueue_style( 'fonts', 'https://fonts.googleapis.com/css2?family=Arimo:wght@400;600&display=swap', array(), _S_VERSION );

	wp_enqueue_script( 'swiper-js', 'https://cdn.jsdelivr.net/npm/swiper@8/swiper-bundle.min.js', array(), _S_VERSION );
    wp_enqueue_script( 'scripts-js', get_template_directory_uri() . '/js/scripts.js', array('swiper-js', 'jquery'), _S_VERSION );

}
add_action( 'wp_enqueue_scripts', 'rav_scripts' );

require get_template_directory() . '/inc/reg-tax.php';
require get_template_directory() . '/inc/reg-post-type.php';

add_action('init', 'my_remove_editor_from_post_type');
function my_remove_editor_from_post_type() {
    remove_post_type_support( 'page', 'editor' );
}

function cc_mime_types( $mimes ){
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
  }
  add_filter( 'upload_mimes', 'cc_mime_types' );