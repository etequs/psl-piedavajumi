<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package rav
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Literata:opsz,wght@7..72,400;7..72,700&display=swap" rel="stylesheet">
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">

	<header id="masthead" class="site-header">
        <div class="content-wrapper">
            <div class="site-branding">
            </div><!-- .site-branding -->

            <div class="nav-toggler"></div>
            <nav id="site-navigation" class="main-navigation">
                <a href="#prieksrocibas">Priekšrocības</a>
                <a href="#modeli">Modeļa versijas</a>
                <a href="#pieteikties">Pieteikties</a>
            </nav><!-- #site-navigation -->
        </div>
	</header><!-- #masthead -->
