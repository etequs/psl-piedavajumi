<?php


function register_theme_custom_post_types() {
    register_post_type( 'model',
        array(
            'labels' => array(
                'name' => __( 'Models' ),
                'singular_name' => __( 'Model' )
            ),
            'public' => true,
            'show_ui' => true,
            'has_archive' => true,
            'taxonomies' => array(''),
            'menu_icon'   => 'dashicons-car',
            'supports' => array('title', 'thumbnail', 'editor')
        )
    );
}

add_action( 'init', 'register_theme_custom_post_types' );