<?php 

add_action( 'init', 'register_theme_campaign_tax' );
add_action( 'init', 'register_theme_engine_type_tax' );

function register_theme_campaign_tax() {
    $labels = array(
        'name'              => _x( 'Campaigns', 'taxonomy general name' ),
        'singular_name'     => _x( 'Campaign', 'taxonomy singular name' ),
        'search_items'      => __( 'Search Campaigns' ),
        'all_items'         => __( 'All Campaigns' ),
        'parent_item'       => __( 'Parent Campaign' ),
        'parent_item_colon' => __( 'Parent Campaign:' ),
        'edit_item'         => __( 'Edit Campaign' ),
        'update_item'       => __( 'Update Campaign' ),
        'add_new_item'      => __( 'Add New Campaign' ),
        'new_item_name'     => __( 'New Campaign Name' ),
        'menu_name'         => __( 'Campaigns' ),
    );
    $args   = array(
        'hierarchical'      => true, // make it hierarchical (like categories)
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => [ 'slug' => 'campaign' ],
    );

    register_taxonomy( 'campaign', [ 'model' ], $args );
}

function register_theme_engine_type_tax() {
    $labels = array(
        'name'              => _x( 'Engine types', 'taxonomy general name' ),
        'singular_name'     => _x( 'Engine type', 'taxonomy singular name' ),
        'search_items'      => __( 'Search Engine types' ),
        'all_items'         => __( 'All Engine types' ),
        'parent_item'       => __( 'Parent Engine type' ),
        'parent_item_colon' => __( 'Parent Engine type:' ),
        'edit_item'         => __( 'Edit Engine type' ),
        'update_item'       => __( 'Update Engine type' ),
        'add_new_item'      => __( 'Add New Engine type' ),
        'new_item_name'     => __( 'New Engine type Name' ),
        'menu_name'         => __( 'Engine types' ),
    );
    $args   = array(
        'hierarchical'      => true, // make it hierarchical (like categories)
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => [ 'slug' => 'engine_type' ],
    );

    register_taxonomy( 'engine_type', [ 'model' ], $args );
}

