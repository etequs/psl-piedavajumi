<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package rav
 */

get_header();
?>

<?php

    $args = array( 
        "post_type" => 'page',
        "post_status" => "publish",
        "posts_per_page" => 1
    );

    $posts = query_posts($args);

    if ( count( $posts ) ) :
        $data = get_fields( $posts[0]->ID );

        get_template_part( 'template-parts/index', 'main-banner', array( "data" => $data['main_banner'] ) );
        get_template_part( 'template-parts/index', 'icons', array( "data" => $data['benefits'] ) );
        get_template_part( 'template-parts/index', 'models', array( "data" => $data['model_list'] ) );
        get_template_part( 'template-parts/index', 'icons-second', array( "data" => $data['included_additional_equipment'] ) );
        get_template_part( 'template-parts/index', 'contact-form', array( "data" => $data['form'] ) );
        get_template_part( 'template-parts/submit_form', null, array( "data" => $data['form'] ) );
    endif;

?>

<?php
get_footer();
