jQuery(document).ready( function($) {
    console.log( 123);
    $('.model-option').click( function() {

        if ($(this).hasClass('active')) return;

        $('.model-option.active').removeClass('active');
        $('.option-entry.active').removeClass('active');

        const fuel_type = $(this).data('fueltype');
        $(`.option-entry.${fuel_type}`).addClass('active');
        $(this).addClass('active');
    });

    $('.submit-form-modal-wrapper').click( function(event){
        if ($(event.target).hasClass('close-modal')) return;
        $(this).find('.hidden-form-wrapper').addClass('is-vis');
        $('.submit-form-modal').addClass('is-vis');
    } );

    $('.submit-form-modal .close-modal').click( function(){
        console.log('click', $(this), $(this).parents('.submit-form-modal-wrapper'), $(this).parents('.submit-form-modal-wrapper').find('.hidden-form-wrapper'));
        $(this).parents('.submit-form-modal-wrapper').find('.hidden-form-wrapper').removeClass('is-vis');
        $('.submit-form-modal').removeClass('is-vis');
    } );
    
    $('.nav-toggler').click(() => $('.main-navigation').toggleClass('active') );

    var SW = new Swiper('.swiper-container', {
        slidesPerView: 1,
        loop: false,
        speed: 400,
        //autoHeight: true,
        simulateTouch: true,
        spaceBetween: 20,
        on: {
            init: function() {
            },
            progress: function(e) {
            },
            slideChange: function(swpr) {
                
            }
        },
        breakpoints: {
            500: {
                slidesPerView: 2,
            },
            768: {
                slidesPerView: 3,
            },
            1100: {
              slidesPerView: 4,
            },
        },
        navigation: {
            nextEl: 'a.swiper-arrow.right',
            prevEl: 'a.swiper-arrow.left',
        },
    });

});