<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package rav
 */

get_header();
?>

    <?php
        $data = get_fields( $post->ID );
    ?>

	<?php get_template_part( 'template-parts/index', 'main-banner', array( "data" => $data['main_banner'] ) ); ?>
	<?php get_template_part( 'template-parts/index', 'icons', array( "data" => $data['benefits'] ) ); ?>
	<?php get_template_part( 'template-parts/index', 'models', array( "data" => $data['model_list'] ) ); ?>
	<?php get_template_part( 'template-parts/index', 'icons-second', array( "data" => $data['included_additional_equipment'] ) ); ?>
	<?php get_template_part( 'template-parts/index', 'contact-form', array( "data" => $data['form'] ) ); ?>
    <?php get_template_part( 'template-parts/submit_form', null, array( "data" => $data['form'] ) ); ?>

<?php
get_footer();