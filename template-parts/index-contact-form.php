<?php

$data = $args['data'];

?>

<section class="simple-block grey-block" id="pieteikties">
    <div class="content-wrapper contact-form">
        <?php if ( isset( $data['title'] ) ) : ?>
            <h2><?php echo $data['title']; ?></h2>
        <?php endif; ?>
        <?php if ( isset( $data['subtitle'] ) ) : ?>
            <p class="subheading"><?php echo $data['subtitle']; ?></p>
        <?php endif; ?>
        <?php echo do_shortcode( $data['form_shortcode'] ); ?>
    </div>
</section>

