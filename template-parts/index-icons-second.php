<?php

$data = $args['data'];

?>

<section class="simple-block" id="prieksrocibas">
    <div class="content-wrapper">
        <div class="data-wrapper icons-in-columns">
            <div class="icons-wrapper icon-wrapper-col">
                <?php if ( isset($data['included']['title'] ) ) : ?>
                    <div class="icon-block-heading m-15"><b><?php echo $data['included']['title']; ?></b></div>
                <?php endif; ?>
                <?php foreach ( $data['included']['items'] as $item ) : ?>
                    <?php if ( gettype($item['icon']) == 'integer' ) {
                        $img_src = wp_get_attachment_url( $item['icon'] );
                    } else {
                        $img_src = $item['icon']['sizes']['thumbnail'];
                    } ?>
                <div class="icon-entry">
                    <div class="icon-wrapper"><img class="icon" src="<?php echo $img_src; ?>"></div>
                    <div class="icon-desc"><?php echo $item['item_description']; ?></div>
                </div>
                <?php endforeach; ?>
                
            </div>
            <div class="icons-wrapper icon-wrapper-col">
            <?php if ( isset($data['additional']['title'] ) ) : ?>
                    <div class="icon-block-heading m-15"><b><?php echo $data['additional']['title']; ?></b></div>
                <?php endif; ?>
                <?php foreach ( $data['additional']['items'] as $item ) : ?>
                <div class="icon-entry">
                    <div class="icon-wrapper"><img class="icon" src="<?php echo $item['icon']['sizes']['thumbnail']; ?>"></div>
                    <div class="icon-desc"><?php echo $item['item_description']; ?></div>
                </div>
                <?php endforeach; ?>
            </div>

        </div>
    </div>
</section>

