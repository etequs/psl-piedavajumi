<?php
    $data = $args['data'];
?>

<section class="simple-block" id="prieksrocibas">
    <div class="content-wrapper">
        <?php if ( isset( $data['block_title'] ) ) : ?>
            <h2><?php echo $data['block_title']; ?></h2>
        <?php endif; ?>
        <div class="data-wrapper">
            <div class="icons-wrapper">
                <?php $index = 1; ?>
                <?php foreach ( $data['benefit_list'] as $benefit ) : ?>
                    <div class="icon-entry">
                        <div class="icon-wrapper digit"><span><?php echo $index; ?></span></div>
                        <div class="icon-desc"><?php echo $benefit['benefit']; ?></div>
                    </div>
                    <?php $index++; ?>
                <?php endforeach; ?>
            </div>

        </div>
    </div>
</section>

