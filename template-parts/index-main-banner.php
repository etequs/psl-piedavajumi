<?php
    $data = $args['data'];
    $background_image = '';
    $video_src = '';
    
    if ( $data['illustration']['illustration_type'] == 'image' ) {
        $background_image = $data['illustration']['image']['sizes']["2048x2048"];
    } else if ( $data['illustration']['illustration_type'] == 'video' ) {
        $video_src = $data['illustration']['video'];
    }

    if ( $data['illustration']['overlay']['overlay_color_1'] && $data['illustration']['overlay']['overlay_color_2'] ) {
        $overlay_style = 'background: linear-gradient(0deg, ' . $data['illustration']['overlay']['overlay_color_1'] . ' 0%, ' . $data['illustration']['overlay']['overlay_color_2'] . ' 100%);';
    }

?>

<section class="main-banner" <?php if ( $background_image && $data['illustration']['illustration_type'] == 'image' ) : ?>style="background-image:url(<?php echo $background_image; ?>)" <?php endif; ?>>
    <?php if ( isset($overlay_style) ) : ?>
        <div class="color-overlay" style="<?php echo $overlay_style; ?>"></div>
    <?php endif; ?>
    <?php if ( $video_src && $data['illustration']['illustration_type'] == 'video' ) : ?>
        <div class="video-bg-wrapper">
            <video src="<?php echo $video_src; ?>" muted autoplay loop></video>
        </div>
    <?php endif; ?>
    <div class="content-wrapper">
        <div class="banner-part left-pt">
            <?php if ( isset( $data['title'] ) ) : ?>
                <p class="block-header"><?php echo $data['title']; ?></p>
            <?php endif; ?>
            <?php if ( isset( $data['subtitle'] ) ) : ?>
                <p class="block-subheader"><?php echo $data['subtitle']; ?></p>
            <?php endif; ?>
        </div>
        <div class="banner-part center-pt">
            <?php if ( isset( $data['text_before_buttons'] ) ) : ?>
                <p
                    class="site-title"
                    <?php if ( isset( $data['text_before_buttons_color'] ) ) { echo ' style="color: ' . $data['text_before_buttons_color'] . ';"'; } ?>
                >
                    <?php echo $data['text_before_buttons']; ?>
                </p>
            <?php endif; ?>
            <div class="btn-wrapper">
                <?php if ( $data['buttons']['go_to_form_button_text'] ) : ?>
                    <div class="btn green-btn"><a href="#pieteikties"></a><?php echo $data['buttons']['go_to_form_button_text']; ?></div>
                <?php endif; ?>
                <?php if ( $data['buttons']['call_now_button_text'] && $data['buttons']['call_now_phone'] ) : ?>
                    <div class="btn">
                        <a href="tel:<?php echo $data['buttons']['call_now_phone']; ?>"></a>
                        <?php echo $data['buttons']['call_now_button_text']; ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>