<?php

$data = $args['data'];
$the_tax = $data['campaign'][0];

$args = array(
    'post_type' => 'model',
    'posts_status' => 'publish',
    'posts_per_page'   => -1,
    'tax_query' => array(
		array(
			'taxonomy' => 'campaign',
			'field'    => 'term_id',
			'terms'    => $the_tax,
		),
	),
);

$all_models = get_posts( $args );

$cars_by_fuel_type = array();

foreach ($all_models as $model) {

    $fuel_type = (wp_get_post_terms( $model->ID, 'engine_type', array( "fields" => "names" )))[0];
    if ( !isset( $cars_by_fuel_type[$fuel_type] ) ) $cars_by_fuel_type[$fuel_type] = array();

    array_push( $cars_by_fuel_type[$fuel_type], $model );
}

$background_img = $data['background']['image'];
if ( $data['background']['overlay']['overlay_color_1'] && $data['background']['overlay']['overlay_color_2'] ) {
    $overlay_style = 'background: linear-gradient(0deg, ' . $data['background']['overlay']['overlay_color_1'] . ' 0%, ' . $data['background']['overlay']['overlay_color_2'] . ' 100%);';
}

?>

<section class="background-block dark model-selector-wrapper" id="modeli"<?php echo $background_img ? 'style="background-image:url(' . $background_img['sizes']['2048x2048'] . ');"' : ''; ?> >
    <?php if ( isset($overlay_style) ) : ?>
        <div class="color-overlay" style="<?php echo $overlay_style; ?>"></div>
    <?php endif; ?>
    <div class="content-wrapper">
        <?php if ( $data['block_title'] ) : ?>
            <h2><?php echo $data['block_title']; ?></h2>
        <?php endif; ?>
        <?php if ( $data['block_subtitle'] ) : ?>
            <p class="subheading"><?php echo $data['block_subtitle']; ?></p>
        <?php endif; ?>
        <div class="model-selector">
            <?php 
            $index = 0;
            foreach ( $cars_by_fuel_type as $key => $value )  {
                echo sprintf('<div class="model-option%s" data-fueltype="%s">%s</div>',
                    !$index ? ' active' : '',
                    $key,
                    $key
            );
                $index++;
            } ?>
        </div>
        <div class="option-block-wrapper gas swiper-container">
            <div class="option-block swiper-wrapper">
                <?php $index=0; ?>
                <?php foreach( $cars_by_fuel_type as $key => $value ) : ?>
                    <?php foreach ( $value as $model ) : ?>
                        <?php $model_data = get_fields($model->ID); ?>
                        <div class="option-entry swiper-slide <?php echo $key; ?><?php echo !$index ? ' active' : ''; ?>">
                            <div class="option-entry-inner">
                                <div class="option-img" style="background-image:url(<?php echo get_the_post_thumbnail_url($model->ID); ?>)"></div>
                                <div class="option-title"><?php echo $model->post_title; ?></div>
                                <div class="engine"><?php echo $key; ?></div>
                                <div class="wheel_drive"><?php echo $model_data['wheel_drive']; ?></div>
                                <div class="option-description">
                                    <?php echo $model->post_content; ?>
                                </div>
                            </div>
                            <div class="option-price">no <?php echo $model_data['price']; ?> EUR/ mēn.*</div>
                        </div>
                    <?php endforeach; ?>
                    <?php $index++; ?>
                <?php endforeach; ?>

            </div>
            
        </div>
        <?php if ( $data['disclaimer'] ) : ?>
            <div class="block-footer"><?php echo $data['disclaimer']; ?></div>
        <?php endif; ?>
    </div>
</section>
                    
                