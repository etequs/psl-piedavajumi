<?php

$data = $args['data'];

?>

<div class="submit-form-modal">
    <div class="submit-form-modal-wrapper">
        <div class="form-caller">
            <p><b>Pieteikties.</b></p>
            <div class="close-modal modal-control"></div>
            <div class="open-modal modal-control"></div>
        </div>
        <div class="hidden-form-wrapper">
            <?php echo do_shortcode( $data['form_shortcode'] ); ?>
        </div>
    </div>
</div>